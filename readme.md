This is for benchmarking the Project Dawn Agents Navigation asset in pure ECS mode (DOTS).

You must own the asset and import it separately.

You will need to select the NavMeshSurface object and press the "Bake" button.

https://assetstore.unity.com/packages/tools/ai/agents-navigation-239233

![screenshot](Images/screenshot.png)