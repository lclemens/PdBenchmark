// this is a static utility class that contains useful entity functions
// its functions are generic for any code using unity packages (does not rely on any externally defined components)
//--------------------------------------------------------------------------------------------------//

using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;

public struct EntityHit
{
    public Entity entity;
    public float distance;
}


public static class EntityUtils
{
    //// fetches the value for the specified singleton type and entity manager.
    //// will throw an error if the instance count of the specified type is not exactly one
    //public static T QuerySingleton<T>(EntityManager em) where T : struct, IComponentData { return em.CreateEntityQuery(ComponentType.ReadOnly<T>()).GetSingleton<T>(); }

    // fetches a previously created singleton value using the default world
    // will throw an error if the instance count of the specified type is not exactly one
    public static T QueryDefaultWorldSingleton<T>() where T : unmanaged, IComponentData
    {
        return World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<T>()).GetSingleton<T>();
    }

    // same as QueryDefaultWorldSingleton(), but with a way to detect a failed query
    public static bool TryQueryDefaultWorldSingleton<T>(out T val) where T : unmanaged, IComponentData
    {
        return World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<T>()).TryGetSingleton(out val);
    }

    // returns the singleton entity with the specified type, and returns Entity.NULL if the query fails
    public static Entity QueryDefaultWorldSingletonEntity<T>() where T : struct, IComponentData
    {
        Entity retEntity = Entity.Null;
        World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<T>()).TryGetSingletonEntity<T>(out retEntity);
        return retEntity;
    }

    // fetches a previously created singleton value using the default world
    // will throw an error if the instance count of the specified type is not exactly one
    public static T QuerySingleton<T>(EntityManager em) where T : unmanaged, IComponentData
    {
        return em.CreateEntityQuery(ComponentType.ReadOnly<T>()).GetSingleton<T>();
    }

    public static T QuerySingleton<T>(ref SystemState state) where T : unmanaged, IComponentData
    {
        return state.GetEntityQuery(ComponentType.ReadOnly<T>()).ToComponentDataArray<T>(Allocator.Temp)[0];
    }

    // same as QueryDefaultWorldSingleton(), but with a way to detect a failed query
    public static bool TryQuerySingleton<T>(EntityManager em, out T val) where T : unmanaged, IComponentData
    {
        return em.CreateEntityQuery(ComponentType.ReadOnly<T>()).TryGetSingleton(out val);
    }

    // returns the singleton entity with the specified type, and returns Entity.NULL if the query fails
    public static Entity QuerySingletonEntity<T>(EntityManager em) where T : struct, IComponentData
    {
        Entity retEntity = Entity.Null;
        em.CreateEntityQuery(ComponentType.ReadOnly<T>()).TryGetSingletonEntity<T>(out retEntity);
        return retEntity;
    }

    // returns the singleton entity with the specified type, and returns Entity.NULL if the query fails
    public static Entity QuerySingletonEntity<T>(ref SystemState state) where T : struct, IComponentData
    {
        return state.GetEntityQuery(ComponentType.ReadOnly<T>()).ToEntityArray(Allocator.Temp)[0];
    }

    //// sets a previously created singleton to the specified value using the dfeault world
    //public static void SetDefaultWorldSingleton<T>(T value) where T : struct, IComponentData { World.DefaultGameObjectInjectionWorld.Systems[0].SetSingleton(value); }

    //// sets a previously created singleton value to the specified value in the specified world
    //public static void SetSingleton<T>(World w, T value) where T : struct, IComponentData { w.Systems[0].SetSingleton(value); }

    // returns true if one or more instances of the specified entity exists in the specified entity manager's world
    public static bool Exists<T>(EntityManager em) { return em.CreateEntityQuery(ComponentType.ReadOnly<T>()).CalculateChunkCount() > 0; }

    // returns true if one or more instances of the specified entity exists in the default world
    public static bool ExistsInDefaultWorld<T>() { return World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<T>()).CalculateChunkCount() > 0; }

    //// this is used to fetch a singleton that was created using a query that has multiple parameters (because of a unity bug)
    //public static T QuerySingletonHack<T>(EntityManager em) where T : struct, IComponentData
    //{
    //    EntityQuery query = em.CreateEntityQuery(ComponentType.ReadOnly<T>());
    //    var arr = query.ToComponentDataArray<T>(Allocator.Temp);
    //    query.CompleteDependency();
    //    var temp = arr[0];
    //    arr.Dispose();
    //    return temp;
    //}

    //// creates a singleton in the specified world with the specified entity manager and set its value to the specified value
    //public static Entity CreateSingleton<T>(EntityManager em, World w, T value) where T : struct, IComponentData
    //{
    //    Entity entity = em.CreateEntity(typeof(T));
    //    SetSingleton(w, value);
    //    #if UNITY_EDITOR
    //        em.SetName(entity, typeof(T).ToString());
    //    #endif
    //    return entity;
    //}

    // creates a singleton in the default world and sets its value to the specified value
    public static Entity CreateDefaultWorldSingleton<T>(T value) where T : unmanaged, IComponentData
    {
        Entity entity = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntity(typeof(T));
        World.DefaultGameObjectInjectionWorld.EntityManager.AddComponentData(entity, value);
        #if UNITY_EDITOR
            World.DefaultGameObjectInjectionWorld.EntityManager.SetName(entity, typeof(T).ToString());
        #endif
        return entity;
    }

    // creates a singleton in the default world
    public static Entity CreateDefaultWorldSingleton<T>() where T : struct, IComponentData
    {
        Entity entity = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntity(typeof(T));
        #if UNITY_EDITOR
            World.DefaultGameObjectInjectionWorld.EntityManager.SetName(entity, typeof(T).ToString());
        #endif
        return entity;
    }


    // create a singleton using the specified entity manager and set its value
    public static Entity CreateSingleton<T>(EntityManager em, T value) where T : unmanaged, IComponentData
    {
        Entity entity = em.CreateEntity(typeof(T));
        em.AddComponentData(entity, value);
        #if UNITY_EDITOR
            em.SetName(entity, typeof(T).ToString());
        #endif
        return entity;
    }


    public static Entity CreateSingleton<T>(EntityManager em) where T : unmanaged, IComponentData
    {
        Entity entity = em.CreateEntity(typeof(T));
        #if UNITY_EDITOR
            em.SetName(entity, typeof(T).ToString());
        #endif
        return entity;
    }

    //// GetSingleton() does not work for dynamic buffers, so this function simulates it by assuming that there is only one instance of the specified type.
    //public static DynamicBuffer<T> QuerySingletonDynamicBuffer<T>(EntityManager em) where T : struct, IBufferElementData
    //{
    //    EntityQuery query = em.CreateEntityQuery(ComponentType.ReadOnly<DynamicBuffer<T>>());
    //    NativeArray<Entity> entities = query.ToEntityArray(Allocator.Temp);
    //    query.CompleteDependency();
    //    Entity entity = entities[0];
    //    entities.Dispose();
    //    return em.GetBuffer<T>(entity);
    //}

    public static EntityManager GetDefaultWorldManager() { return World.DefaultGameObjectInjectionWorld.EntityManager; }

    public static void AddComponentData<T>(EntityManager em, EntityQuery entityQuery, T component) where T : unmanaged, IComponentData
    {
        // one way to do it.... probably slow as hell...
        var entities = entityQuery.ToEntityArray(Allocator.TempJob);
        for (int i = 0; i < entities.Length; i++) {
            em.AddComponentData(entities[i], component);
        }

        //// ANOTHER WAY TO DO IT
        //int count = entityQuery.CalculateEntityCount();
        //NativeArray<T> array = new NativeArray<T>(count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
        //for (int i = 0; i < count ; i++) { array[i] = component; }
        //em.AddComponentData(entityQuery, array);
    }

    public static void AddComponentData<T>(EntityManager em, in NativeArray<Entity> entities, T component) where T : unmanaged, IComponentData
    {
        for (int i = 0; i < entities.Length; i++) {
            em.AddComponentData(entities[i], component);
        }
    }

    //// places the specified entity at the specified position
    //static public void SetPosition(EntityManager em, in Entity entity, in float3 position)
    //{
    //    em.SetComponentData<Translation>(entity, new Translation { Value = position });
    //}

    //// attaches the child to the parent while ensuring that LocalToParent is on the child (EntityManger version)
    //public static void SetParent(EntityManager em, in Entity childEntity, in Entity parentEntity, in Entity root)
    //{
    //    em.AddComponentData(childEntity, new Parent { Value = parentEntity });
    //    em.AddComponentData(childEntity, new LocalToParent());
    //    DynamicBuffer<LinkedEntityGroup> buf = em.GetBuffer<LinkedEntityGroup>(root);
    //    buf.Add(new LinkedEntityGroup { Value = childEntity });
    //}

    //// attaches the child to the parent (EntityManger version).
    //// note that this overload does not link the entity group
    //public static void SetParentWithoutLinking(EntityManager em, in Entity childEntity, in Entity parentEntity)
    //{
    //    em.AddComponentData(childEntity, new Parent { Value = parentEntity });
    //    em.AddComponentData(childEntity, new LocalToParent());
    //}

    //// attaches the child to the parent while ensuring that LocalToParent is on the child (ecb version)
    //public static void SetParent(in EntityCommandBuffer.ParallelWriter ecb, int entityInQueryIndex, in Entity child, in Entity parent, in Entity root)
    //{
    //    ecb.AddComponent(entityInQueryIndex, child, new Parent { Value = parent });
    //    ecb.AddComponent(entityInQueryIndex, child, new LocalToParent());
    //    ecb.AppendToBuffer<LinkedEntityGroup>(entityInQueryIndex, root, new LinkedEntityGroup { Value = child });
    //}

    //// attaches the child to the parent while ensuring that LocalToParent is on the child (ecb version)
    //// NOTE!! this overload does not link entity groups
    //public static void SetParent(in EntityCommandBuffer.ParallelWriter ecb, int entityInQueryIndex, in Entity child, in Entity parent)
    //{
    //    ecb.AddComponent(entityInQueryIndex, child, new Parent { Value = parent });
    //    ecb.AddComponent(entityInQueryIndex, child, new LocalToParent());
    //}

    //// copies the specified component from the specified source entity to the specified destination entity
    //public static void CopyComponent<T>(EntityManager em, in Entity source, in Entity destination) where T : struct, IComponentData
    //{
    //    if (!em.HasComponent<T>(source)) { return; }
    //    var data = em.GetComponentData<T>(source);
    //    em.AddComponentData(source, data);
    //}
}