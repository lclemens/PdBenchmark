using Unity.Entities;
using UnityEngine;
using Unity.Mathematics;
using TMPro;
using Unity.Collections;
using Unity.Transforms;
using ProjectDawn.Navigation;

public enum AvoidanceAlg { None, Collider, Separation, Reciporical, Sonar, Physics, ColliderAndSeparation }

public class SpawnPanelController : MonoBehaviour
{
    public TMP_InputField SpawnCountInput;
    public TextMeshProUGUI StatusText;
    int m_totalSpawnCount = 0;

    public float SeparationRadius = AgentSeparation.Default.Radius;
    public float SeparationWeight = AgentSeparation.Default.Weight;
    public float ReciprocalRadius = AgentReciprocalAvoid.Default.Radius;
    public float SonarRadius = AgentSonarAvoid.Default.Radius;
    public float SonarMaxAngle = AgentSonarAvoid.Default.MaxAngle;
    public float SonarAngle = AgentSonarAvoid.Default.Angle; //2.35619f; // 135 degrees;
    public bool SonarBlockedStop = AgentSonarAvoid.Default.BlockedStop; // use false with collider, and true without one
    public SonarAvoidMode SonarMode = AgentSonarAvoid.Default.Mode; //SonarAvoidMode.IgnoreBehindAgents;
    public NavigationLayers Layers = NavigationLayers.Everything;

    public AvoidanceAlg CurrentAlgorithm = AvoidanceAlg.None;

    public bool m_isSmartStopEnabled = false;

    public void PerformClear()
    {
        EntityManager em = EntityUtils.GetDefaultWorldManager();
        EntityQuery query = em.CreateEntityQuery(ComponentType.ReadOnly<ProjectDawn.Navigation.AgentBody>());
        em.DestroyEntity(query);
        m_totalSpawnCount = 0;
        StatusText.text = $"0";
    }

    public void SwitchAvoidanceAlgorithm(int algIndex)
    {
        AvoidanceAlg alg = (AvoidanceAlg)algIndex;
        EntityManager em = EntityUtils.GetDefaultWorldManager();
        EntityQuery query = em.CreateEntityQuery(ComponentType.ReadOnly<ProjectDawn.Navigation.AgentBody>());
        em.RemoveComponent<AgentReciprocalAvoid>(query);
        em.RemoveComponent<AgentCollider>(query);
        em.RemoveComponent<AgentSeparation>(query);
        em.RemoveComponent<AgentSonarAvoid>(query);
        //em.RemoveComponent<PhysicsWorldIndex>(query);
        var entities = query.ToEntityArray(Allocator.TempJob);
        PhysicsUtils.RemovePhysicsComponents(em, entities);
        switch (alg) {
            case AvoidanceAlg.Collider:
                EntityUtils.AddComponentData(em, entities, new AgentCollider { Layers = Layers });
                break;
            case AvoidanceAlg.Separation:
                EntityUtils.AddComponentData(em, entities, new AgentSeparation { Radius = SeparationRadius, Weight = SeparationWeight, Layers = Layers } );
                break;
            case AvoidanceAlg.Reciporical: 
                EntityUtils.AddComponentData(em, entities, new AgentReciprocalAvoid { Radius = ReciprocalRadius, Layers = Layers });
                break;
            case AvoidanceAlg.Sonar:
                EntityUtils.AddComponentData(em, entities, new AgentSonarAvoid { Angle = SonarAngle, Radius = SonarRadius, BlockedStop = SonarBlockedStop, Mode = SonarMode, Layers = Layers, MaxAngle = SonarMaxAngle }); 
                EntityUtils.AddComponentData(em, entities, new AgentCollider { Layers = Layers });
                break;
            case AvoidanceAlg.ColliderAndSeparation: 
                EntityUtils.AddComponentData(em, entities, new AgentSeparation { Radius = SeparationRadius, Weight = SeparationWeight, Layers = Layers });
                EntityUtils.AddComponentData(em, entities, new AgentCollider { Layers = Layers });
                break;
            case AvoidanceAlg.Physics:
                PhysicsUtils.AddPhysicsComponents(em, entities, PhysicsUtils.MakeCapsuleCollider()); 
                break;
        }
        CurrentAlgorithm = alg;
    }

    public void PerformSpawn()
    {
        EntityManager em = EntityUtils.GetDefaultWorldManager();

        Entity spawnerEntity = EntityUtils.QueryDefaultWorldSingletonEntity<AgentSpawnerOpts>();

        AgentSpawnerOpts spawnerOpts = em.GetComponentData<AgentSpawnerOpts>(spawnerEntity);
        LocalToWorld spawnerLtw = em.GetComponentData<LocalToWorld>(spawnerEntity);

        int spawnCount = int.Parse(SpawnCountInput.text);

        NativeArray<Entity> entities = em.Instantiate(spawnerOpts.PrefabEntity, spawnCount, Allocator.Temp);

        float3 pos = spawnerLtw.Position;

        float spacing = 1f;

        Entity destEntity = EntityUtils.QuerySingletonEntity<DestinationTag>(em);
        LocalToWorld ltwDest = em.GetComponentData<LocalToWorld>(destEntity);

        for (int i = 0; i < spawnCount; i++) {
            Entity spawnedEntity = entities[i];
            LocalTransform lt = em.GetComponentData<LocalTransform>(spawnedEntity);
            if ((i % 100) == 0) { 
                pos.z += spacing;
                pos.x = spawnerLtw.Position.x;
            }
            pos.x += spacing;
            lt.Position = pos;
            em.SetComponentData(spawnedEntity, lt);

            // set the algorithm
            switch (CurrentAlgorithm) {
                case AvoidanceAlg.Collider: 
                    em.AddComponentData(spawnedEntity, new AgentCollider { Layers = Layers });
                    break;
                case AvoidanceAlg.Separation: 
                    em.AddComponentData(spawnedEntity, new AgentSeparation { Radius = SeparationRadius, Weight = SeparationWeight, Layers = Layers });
                    break;
                case AvoidanceAlg.Reciporical:
                    em.AddComponentData(spawnedEntity, new AgentReciprocalAvoid { Radius = ReciprocalRadius, Layers = Layers });
                    break;
                case AvoidanceAlg.Sonar: 
                    em.AddComponentData(spawnedEntity, new AgentSonarAvoid { Angle = SonarAngle, Radius = SonarRadius, Mode = SonarMode, BlockedStop = SonarBlockedStop, MaxAngle = SonarMaxAngle, Layers = Layers }); 
                    em.AddComponentData(spawnedEntity, new AgentCollider { Layers = Layers });
                    break;
                case AvoidanceAlg.Physics:
                    PhysicsUtils.AddPhysicsComponents(em, spawnedEntity, PhysicsUtils.MakeCapsuleCollider());
                    break;
                case AvoidanceAlg.ColliderAndSeparation:
                    em.AddComponentData(spawnedEntity, new AgentSeparation { Radius = SeparationRadius, Weight = SeparationWeight, Layers = Layers });
                    em.AddComponentData(spawnedEntity, new AgentCollider { Layers = Layers });
                    break;
            }

            // smart stop
            em.SetComponentEnabled<AgentSmartStop>(spawnedEntity, m_isSmartStopEnabled);
            
            // change the destination
            AgentBody body = em.GetComponentData<AgentBody>(spawnedEntity);
            body.SetDestination(ltwDest.Position);
            em.SetComponentData(spawnedEntity, body);
        }

        m_totalSpawnCount += spawnCount;

        StatusText.text = $"{m_totalSpawnCount}";

        entities.Dispose();
    }

    public void ToggleSmartStop(bool enable)
    {
        m_isSmartStopEnabled = enable;

        var queryDesc = new EntityQueryDesc
        {
            All = new ComponentType[] { ComponentType.ReadOnly<AgentSmartStop>() },
            Options = EntityQueryOptions.IgnoreComponentEnabledState
        };
        EntityManager em = EntityUtils.GetDefaultWorldManager();
        EntityQuery query = em.CreateEntityQuery(queryDesc);
        em.SetComponentEnabled<AgentSmartStop>(query, enable);
    }
}
