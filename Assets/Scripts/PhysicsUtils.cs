// This is a helper class that contains useful physics functions
//-------------------------------------------------------------------------------//

using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Collections;

public static class PhysicsUtils
{
    public static CollisionFilter MakeEverythingFilter()
    {
        CollisionFilter filter = CollisionFilter.Default;
        filter.CollidesWith = uint.MaxValue;
        filter.BelongsTo = uint.MaxValue;
        return filter;
    }

    // set the collision filter for the specified collider
    // returns the previous collides-with value
    public static uint SetCollisionFilter(in PhysicsCollider collider, uint collidesWith = uint.MaxValue)
    {
        uint ret = 0;
        unsafe {
            CollisionFilter filter = collider.ColliderPtr->GetCollisionFilter();
            ret = filter.CollidesWith;
            filter.CollidesWith = collidesWith;
            collider.ColliderPtr->SetCollisionFilter(filter);
        }
        return ret;
    }

    public static PhysicsCollider MakeCapsuleCollider()
    {
        CollisionFilter filter = MakeEverythingFilter();
        Material mat = new Material
        {
            CollisionResponse = CollisionResponsePolicy.Collide,
            EnableMassFactors = true,
            EnableSurfaceVelocity = false,
            CustomTags = 0,
            Friction = 0.5f,
            FrictionCombinePolicy = Material.CombinePolicy.GeometricMean,
            Restitution = 0f,
            RestitutionCombinePolicy = Material.CombinePolicy.Maximum
        };
        CapsuleGeometry capsule = new CapsuleGeometry { Radius = 0.5f, Vertex0 = new float3(0f, -1f, 0f), Vertex1 = new float3(0f, 1f, 0f)};
        return new PhysicsCollider { Value = CapsuleCollider.Create(capsule, filter, mat) };
    }

    public static void AddPhysicsComponents(EntityManager em, Entity entity, in PhysicsCollider collider)
    {
        em.AddComponentData(entity, collider);
        em.AddComponentData(entity, new PhysicsDamping { Angular = 2f, Linear = 1f });
        em.AddComponentData(entity, PhysicsMass.CreateDynamic(collider.MassProperties, 1f));
        //em.AddComponentData(entity, new PhysicsMass { Transform = new RigidTransform { pos = new float3(0,0,0), rot = new quaternion(0,0,0,1) }, AngularExpansionFactor = 0, InverseInertia = new float3(10,10,10), InverseMass = 1f });
        em.AddComponent<PhysicsVelocity>(entity);
        em.AddComponent<PhysicsWorldIndex>(entity);
    }

    public static void AddPhysicsComponents(EntityManager em, in NativeArray<Entity> entities, in PhysicsCollider collider)
    {
        for (int i = 0; i < entities.Length; i++) {
            AddPhysicsComponents(em, entities[i], collider);
        }
    }

    public static void RemovePhysicsComponents(EntityManager em, Entity entity)
    {
        em.RemoveComponent<PhysicsCollider>(entity);
        em.RemoveComponent<PhysicsDamping>(entity);
        em.RemoveComponent<PhysicsMass>(entity);
        em.RemoveComponent<PhysicsVelocity>(entity);
        em.RemoveComponent<PhysicsWorldIndex>(entity);
    }

    public static void RemovePhysicsComponents(EntityManager em, in NativeArray<Entity> entities)
    {
        for (int i = 0; i < entities.Length; i++) {
            RemovePhysicsComponents(em, entities[i]);
        }
    }
}